# MBZIRC Deck Tracking Vision System #

### What is this repository for? ###

This code provides the detection and tracking of the deck on the moving truck for MBZIRC Challenge.

### How do I get set up? ###

To use the code on the on-board Manifold computer, please consult the [Launch Readme](https://bitbucket.org/castacks/mbzirc_launch).

To use the code in the simulation environment, please consult the [Simulation Readme](https://bitbucket.org/castacks/mbzirc_simulation).

**Important:** If you receive compilation error for *nonfree* OpenCV module, install this module using the following commands:

```
#!bash

sudo add-apt-repository --yes ppa:xqms/opencv-nonfree
sudo apt-get update 
sudo apt-get install libopencv-nonfree-dev
```

### Who do I talk to? ###

* Guilherme Pereira (guilhermepereira)
* Azarakhsh Keipour (keipour@gmail.com)