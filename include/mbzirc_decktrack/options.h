#ifndef __OPTIONS_INCLUDED__
#define __OPTIONS_INCLUDED__ 

//#define SHOWCONTOURSONEBYONE 		// Shows contours in an image one by one and prints info about it
//#define SHOWDETECTIONS		// Show all detected candidate targets and their contours on the frame
//#define SHOWEDGEIMAGE			// Show edge image
#define SAVEIMAGES			// Save all processed frames into numbered files
//#define SAVEONLYDETECTEDIMAGES	// Save only frames where the target is found into numbered files
//#define SHOWHISTOGRAM			// Show histogram of the detected area
//#define SHOWDETECTEDAREA		// Show the detected area in a separate image
//#define SHOWPROCESSINGTIME		// Show time for processing each frame and the frequency of program output
#define DRAWDETECTION			// Draw the detection on the frame image
#define IMAGESEQUENCE           // Read the input from an image sequence

int CALIBRATEPARAMETERS = 0;		// Show calibration GUI for the vision subsystem

# endif // __OPTIONS_INCLUDED__
