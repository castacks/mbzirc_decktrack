#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>

class Altimeter
{
  bool gotData_;
  double height_;

public:
   Altimeter(ros::NodeHandle n)
   { 
      gotData_ = false;
      height_ = 0;
   }
  
  void LaserCB(const sensor_msgs::LaserScan::ConstPtr &msg)
  {
    gotData_=true;

    if (msg->intensities[0]==1 && msg->ranges[0]>0.0)
    { // Only valid values are considered
        height_=msg->ranges[0];
    }
  }

  bool gotData()
  {
     return gotData_;
  }
  
  double height()
  {
     return height_;
  }

};

