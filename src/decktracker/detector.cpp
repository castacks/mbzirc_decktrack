#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/highgui/highgui.hpp>
#include <mbzirc_commons/commons.h>
#include <mbzirc_decktrack/options.h>

namespace mbzirc 
{
namespace decktracker
{

class Detector
{
private:
  
  // Black canvas for the essential operations
  cv::Mat black_canvas_;
  std::vector<commons::DetectionResult> DetectedCircles;
  

  void DetectCross(commons::EdgeImage &edges, cv::Rect roi, commons::Ellipse detection, int border_size = 15)
  {
    if (mbzirc::commons::DebugLevel::ShowImages)
    {
      LOG_INFO("Detection: x = %0.1f, y = %0.1f, width = %0.1f, height = %0.1f", detection.center.x, detection.center.y, detection.size.width, detection.size.height);
      
      // Compensate the offset
      detection.center.x -= roi.x;
      detection.center.y -= roi.y;
      LOG_INFO("Compensated detection: x = %0.1f, y = %0.1f, width = %0.1f, height = %0.1f", detection.center.x, detection.center.y, detection.size.width, detection.size.height);

      // Extract the interior of the target
      LOG_INFO("Extracting ellipse...");
      cv::Mat img_interior;
      detection.ExtractEllipseFromImage(edges.Image, img_interior, true, true, border_size);
      LOG_INFO("Ellipse extracted...");
      
      
      
      cv::imshow("Cross", img_interior);
      cv::waitKey(1);
    }
  }
  
    bool VerifyTarget(int &detection_number)
    {
        // Verify the detection (for the new target) by checking if there is another ellipse with a near center
        LOG_INFO("Starting target verification...");
        double max_distance = cv::max(DetectedCircles[detection_number].ResultRect.size.width, DetectedCircles[detection_number].ResultRect.size.height) / 5.0F;
        bool verified = false;
        for (int i = 0; i < DetectedCircles.size(); ++i)
        {
            if (i == detection_number) continue;
            if (cv::norm(DetectedCircles[i].ResultRect.center - DetectedCircles[detection_number].ResultRect.center) < max_distance)
            {
                if (DetectedCircles[i].ResultRect.size.width > DetectedCircles[detection_number].ResultRect.size.width)
                    detection_number = i;
                verified = true;
            }
        }
        if (DetectedCircles[detection_number].ResultRect.size.width > 300 && DetectedCircles[detection_number].ResultRect.size.height > 300)
            verified = true;
        LOG_INFO("Finished target verification...");

        return verified;
    }

  void ShowCircleContourInfo(commons::EdgeImage &edges, commons::Ellipse &ellipse, int contour_number)
  {
    cv::Mat contour_image;
    cv::cvtColor(edges.Image, contour_image, CV_GRAY2RGB);

    cv::drawContours(contour_image, edges.Contours, contour_number, cv::Scalar(255, 255, 255), 2, 8, cv::noArray(), 0, cv::Point());
    cv::ellipse(contour_image, ellipse, cv::Scalar(0, 0, 255), 1, 8);
    cv::imshow("Contours", contour_image);
    ROS_INFO("%i: Size = %zu Width=%0.1f  Height=%0.1f  Angle=%0.2f", contour_number, edges.Contours[contour_number].size(), ellipse.size.width, ellipse.size.height, ellipse.angle);
    cv::waitKey(0);
  }
  
  void DetectEllipses(commons::EdgeImage &edges, cv::Mat mask,
    cv::Point offset, double max_axis_ratio, int min_axis_length, int max_axis_length, int min_ellipse_points, 
    double min_contour_fit_score, double min_ellipse_fit_score)
  {
    
    #ifdef SHOWCONTOURSONEBYONE
      ROS_INFO("Total contours: %lu", edges.Contours.size());
      ROS_INFO("Max axis ratio: %0.1lf\tMin axis length: %i\tMin points: %i", max_axis_ratio, min_axis_length, min_ellipse_points);
    #endif
    
    // Initialize the output
    DetectedCircles.clear();
    
    
    for (int i = 0; i < edges.GoodContours.size(); i++)
    {
      // Fit ellipse to the contour      
      commons::Ellipse ellipse;
      bool isEllipse = ellipse.CreateFromContour(edges.Contours[edges.GoodContours[i]], 
						 min_ellipse_points, max_axis_ratio, min_axis_length, max_axis_length);

      #ifdef SHOWCONTOURSONEBYONE
        ShowCircleContourInfo(edges, ellipse, edges.GoodContours[i]);
      #endif
      
      // Go to the next contour if is not an ellipse
      if (!isEllipse) continue;
      
      // Draw the ellipse on a canvas
      ellipse.DrawEllipse(mask, cv::Scalar(255));

      // Calculate the score of the pixels in the contour overlapped with the ellipse
      std::vector<double> ellipse_scores(2);
      ellipse_scores[0] = ellipse.OverlapContourOnEllipse(edges.Contours[edges.GoodContours[i]], mask);

      #ifdef SHOWCONTOURSONEBYONE
        ROS_INFO("Score1 = %0.1f", ellipse_scores[0]);
        cv::waitKey(0);
      #endif

      // Reject if score1 is too low
      if (ellipse_scores[0] < min_contour_fit_score) 
      {
        // Remove the ellipse from the canvas
        ellipse.DrawEllipse(mask, cv::Scalar(0));
        continue;
      }

      // Calculate the score of the pixels in the ellipse overlapped with the edges
      ellipse_scores[1] = ellipse.OverlapEllipseOnEdgeImage(mask, edges.BoldImage);

      // Remove the ellipse from the canvas
      ellipse.DrawEllipse(mask, cv::Scalar(0));

      #ifdef SHOWCONTOURSONEBYONE
        ROS_INFO("Score2 = %0.1f", ellipse_scores[1]);
        cv::waitKey(0);
      #endif

      // Reject if score1 is too low
      if (ellipse_scores[1] < min_ellipse_fit_score) continue;

      // Correct the ellipse offset
      ellipse.center.x += offset.x;
      ellipse.center.y += offset.y;

      // Push the ellipse to the queue
      DetectedCircles.push_back(commons::DetectionResult(edges.GoodContours[i], 
							  ellipse_scores, ellipse));
    }
  }
  
  void ShowDetections(cv::Mat &img_frame, cv::vector<commons::DetectionResult> &Detections, commons::EdgeImage &edges, cv::Point offset)
  {
    LOG_INFO("Starting showing detections...");

    for (int i = 0; i < Detections.size(); ++i)
    {
      // Show info about the detection
      if (commons::DebugLevel::PrintInfo)
      {
	ROS_INFO("Detection %i: Score1=%0.2lf  Score2=%0.2lf  Score3=%0.2lf Score4=%0.2lf Width=%0.1f  Height=%0.1f  Angle=%0.2f", 
		i, Detections[i].Scores[0], Detections[i].Scores[1], Detections[i].Scores[2], Detections[i].Scores[3],
		Detections[i].ResultRect.size.width, Detections[i].ResultRect.size.height, 
		Detections[i].ResultRect.angle);
      }
      
      if (commons::DebugLevel::ShowImages)
      {
	cv::drawContours(img_frame, edges.Contours, Detections[i].Tag, cv::Scalar(100, 0, 100), 2, 8, cv::noArray(), 0, offset);
	cv::ellipse(img_frame, Detections[i].ResultRect, cv::Scalar(0, 255, 0), 2, 8);
	char detnumber[10]; sprintf(detnumber, "%i", i);
	commons::OpenCVImageTools::PrintTextOnImage(img_frame, detnumber, Detections[i].ResultRect.center, cv::Scalar(0,0,100), 1);
	cv::imshow("Detections", img_frame);
	cv::waitKey(1);
      }
    }
    
    LOG_INFO("Finished showing detections...");
  }
  
  double CalculateWhitenessScore(cv::Mat img_frame, cv::Mat mask, commons::Ellipse detection, int whiteness_threshold)
  {
    LOG_INFO("Starting whiteness calculation...");

    // Create the canvas image from ellipse
    detection.DrawEllipse(mask, cv::Scalar(255), true);
    
    // Calculate the histogram
    cv::MatND hist = commons::OpenCVImageTools::CalculateHistogram(img_frame, mask, true);
    
    // Clear the canvas image
    detection.DrawEllipse(mask, cv::Scalar(0), true);

    // Calculate the fraction of pixels above the threshold
    double score = 1.0F - ((double)hist.at<float>(whiteness_threshold) / hist.at<float>(255));
    
    LOG_INFO("Finished whiteness calculation...");
    
    return score;
  }
  
public: 
  
    bool DetectTarget(
        cv::Mat &img_frame, cv::RotatedRect &target_rect, cv::Rect roi, double max_axis_ratio, int min_axis_length, int max_axis_length,
        int min_ellipse_points, double min_contour_fit_score, double min_ellipse_fit_score, 
        double min_white_pixels_ratio, double max_edge_pixels_ratio, int whiteness_threshold)
    {
        LOG_INFO("Starting target detection...");

        try
        {
            // Initialize the canvas
            if (black_canvas_.empty())
                black_canvas_ = cv::Mat::zeros(img_frame.size(), CV_8UC1);

            // Calculate the edges in the image
            commons::EdgeImage edges;
            if (roi.width > 0)
                edges.CalculateEdges(img_frame(roi), 50, 200, 5, min_ellipse_points, 2);
            else
                edges.CalculateEdges(img_frame, 50, 200, 5, min_ellipse_points, 2);


            #ifdef SHOWEDGEIMAGE
                // Show the edges
                if (commons::DebugLevel::ShowImages)
                {
                    cv::imshow("Edges", edges.Image);
                    cv::waitKey(1);
                }
            #endif

            // Detect the candidate circles
            LOG_INFO("Starting circle detection...");
            if (roi.width > 0)
                this->DetectEllipses(edges, black_canvas_(roi), cv::Point(roi.x, roi.y), max_axis_ratio, 
                    min_axis_length, max_axis_length, min_ellipse_points, min_contour_fit_score, min_ellipse_fit_score);
            else if (black_canvas_.cols == edges.Image.cols)
                this->DetectEllipses(edges, black_canvas_, cv::Point(0, 0), max_axis_ratio, 
                    min_axis_length, max_axis_length, min_ellipse_points, min_contour_fit_score, min_ellipse_fit_score);
            else
            {
                this->DetectEllipses(edges, black_canvas_(cv::Rect(0, 0, edges.Image.size().width, edges.Image.size().height)), cv::Point(0, 0), max_axis_ratio, 
                    min_axis_length, max_axis_length, min_ellipse_points, min_contour_fit_score, min_ellipse_fit_score);
            }

            LOG_INFO("Finished circle detection...");

            if (DetectedCircles.empty())
            {
                LOG_INFO("No candidate circles found...");
                return false;
            }

            // Calculate whiteness scores
            LOG_INFO("Starting whiteness calculation...");
            for (int i = 0; i < DetectedCircles.size(); ++i)
            { 
                cv::RotatedRect ellipse_rect = DetectedCircles[i].ResultRect;
                ellipse_rect.center.x -= roi.x;
                ellipse_rect.center.y -= roi.y;
                if (roi.width > 0)
                    DetectedCircles[i].Scores[2] = (CalculateWhitenessScore(img_frame(roi), black_canvas_(roi), ellipse_rect, whiteness_threshold));
                else if (black_canvas_.cols == edges.Image.cols)
                    DetectedCircles[i].Scores[2] = (CalculateWhitenessScore(img_frame, black_canvas_, ellipse_rect, whiteness_threshold));
                else
                    DetectedCircles[i].Scores[2] = (CalculateWhitenessScore(img_frame, black_canvas_(cv::Rect(0, 0, img_frame.size().width, img_frame.size().height)), ellipse_rect, whiteness_threshold));
            }
            LOG_INFO("Finished whiteness calculation...");

            #ifdef SHOWDETECTIONS
                cv::Mat img_detections;
                img_frame.copyTo(img_detections);
                ShowDetections(img_detections, DetectedCircles, edges, cv::Point(roi.x, roi.y));
            #endif

            // Find the most promising detection
            double combined_score = 0;
            int detection_number = -1;
            for (int i = 0; i < DetectedCircles.size(); ++i)
            {
                double ellipse_score = DetectedCircles[i].Scores[0] * DetectedCircles[i].Scores[1];
                if (DetectedCircles[i].Scores[2] > min_white_pixels_ratio && combined_score < ellipse_score)
                {
                    combined_score = ellipse_score;
                    // target_rect = DetectedCircles[i].ResultRect;
                    detection_number = i;
                }
            }

            bool target_verified = false;
            if (detection_number >= 0)
            {
                // Verify the target
                target_verified = VerifyTarget(detection_number);
                
                // Specify the output rectangle
                target_rect = DetectedCircles[detection_number].ResultRect;

                LOG_INFO("Starting cross detection...");
                //DetectCross(edges, roi, target_rect, 3);
                LOG_INFO("Finished cross detection...");
            }

            return detection_number >= 0 && target_verified;
        }
        catch (int e)
        {
            LOG_ERROR("Error in target detection...");
            ROS_ERROR("Detector exception #%i", e);
            return false;
        }
  }
  
};

} // end namespace decktracker

} // end namespace mbzirc
