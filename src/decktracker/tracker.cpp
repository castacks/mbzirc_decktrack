#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/highgui/highgui.hpp>
#include <string>
#include <fstream>
#include <mbzirc_commons/commons.h>
#include "detector.cpp"
#include <mbzirc_decktrack/options.h>
#include <mbzirc_decktrack/laser.h>

namespace mbzirc 
{
namespace decktracker
{
  
class Tracker
{
private:
  cv::RotatedRect target_rect_;
  int tracking_remained_frames_; // The number of frames to track when nothing is found
  int frame_scale_factor;
  int frame_current_scale;
  int scale_size_threshold;
  int initial_frame_scale;
  Detector dtd;
  std::string calib_output_filename_;
  
  // Parameters -------------------------------------------------
  
  double min_contour_fit_score_detect_; 	// Confidence to claim detection when the deck is not tracked
  double min_contour_fit_score_track_;  	// Confidence to claim detection when the deck is tracked
  double min_ellipse_fit_score_detect_;		// Confidence to claim detection when the deck is not tracked
  double min_ellipse_fit_score_track_;  	// Confidence to claim detection when the deck is tracked
  double min_white_pixels_ratio_detect_;	// Whiteness score for detection
  double min_white_pixels_ratio_track_; 	// Whiteness score for tracking
  double max_edge_pixels_ratio_detect_;
  double max_edge_pixels_ratio_track_;
  int whiteness_threshold_;
  int max_size_change_;        	 		// Maximum change in size while tracking
  int track_frames_;            	 	// Number of frames to keep tracking when the target is not found

  // Trackbar parameters
  
  int min_contour_fit_score_detect;
  int min_contour_fit_score_track;
  int min_ellipse_fit_score_detect;
  int min_ellipse_fit_score_track;
  int min_white_pixels_ratio_detect;
  int min_white_pixels_ratio_track;
  int max_edge_pixels_ratio_detect;
  int max_edge_pixels_ratio_track;
  int whiteness_threshold;
  int max_size_change;
  int track_frames;
  
  // Functions --------------------------------------------------
    
  // Tracks a circle target with a big cross in it. Returns true if the call is successfull
  bool TrackTarget(cv::Mat &img_frame, 
      cv::RotatedRect &result_rect, int &detected_scale)    	 // Result of the tracked target. Only valid when the function returns true
  {
      double axis_ratio_detect = 5.0F;
      double axis_ratio_track = 5.0F;
      int min_axis_size = 5;
      int max_axis_size = 700;
      int min_pixel_count = 50;
      detected_scale = -1;
      
      // Change the minimum and maximum target size due to the height
      if (Laser.gotData() && Laser.height() > 6.0F)
          max_axis_size = 100;
      else if (Laser.gotData() && Laser.height() > 3.0F)
          max_axis_size = 100 + std::floor((6.0F - Laser.height()) * 50);
      
      if (Laser.gotData() && Laser.height() < 0.5F)
          min_axis_size = 200;
      else if (Laser.gotData() && Laser.height() < 1.5F)
          min_axis_size = 100;
      
    bool isDetected;
    if (tracking_remained_frames_ == 0) // If in the detection mode
    {
      LOG_INFO("Entering detection mode...");

      // Detect the target candidate circles
      LOG_INFO("Starting target detection...");
      
      for (int iter = 0; iter < 10; ++iter)
      {
        // Scale the frame
        cv::Mat img_frame_scaled;
        if (frame_current_scale != 1)
            cv::resize(img_frame, img_frame_scaled, cv::Size(img_frame.size().width / frame_current_scale, img_frame.size().height / frame_current_scale));
        else
            img_frame.copyTo(img_frame_scaled);
        
        isDetected = dtd.DetectTarget(img_frame_scaled, result_rect, cv::Rect(), axis_ratio_detect, min_axis_size, max_axis_size, min_pixel_count, 
            min_contour_fit_score_detect_, min_ellipse_fit_score_detect_, min_white_pixels_ratio_detect_, 
            max_edge_pixels_ratio_detect_, whiteness_threshold_);
        
        int last_frame_scale = frame_current_scale;
        
            if (isDetected && result_rect.size.height > scale_size_threshold)
                frame_current_scale *= frame_scale_factor;
            else if ((!isDetected || result_rect.size.height < scale_size_threshold / frame_scale_factor) && frame_current_scale > initial_frame_scale)
                frame_current_scale /= frame_scale_factor;
            
            if (isDetected) 
            {
                // Rescale the final result (target) to the real size
                result_rect.center.x *= last_frame_scale;
                result_rect.center.y *= last_frame_scale;
                result_rect.size.height *= last_frame_scale;
                result_rect.size.width *= last_frame_scale;
                detected_scale = last_frame_scale;
                break;
            }
            
            if (last_frame_scale == initial_frame_scale)
                break;
      }
              
        LOG_INFO("Finished target detection...");
    }
    else // if in the tracking mode
    {
      LOG_INFO("Entering tracking mode...");

      tracking_remained_frames_--;
      int size_thresh = std::min(result_rect.size.width, result_rect.size.height) - max_size_change_;
      size_thresh = std::max(size_thresh, min_axis_size);
      
      // Setup a rectangle to define the region of interest
      cv::Rect roi = ((commons::Ellipse)result_rect).BoundingRectAdjusted(img_frame.size().width, 
									  img_frame.size().height);

      // Maximum distance from the edges of previous detection to search for the new detection
      int distance_thresh = 20;
      int x2 = cv::min(roi.x + roi.width + distance_thresh, img_frame.size().width);
      int y2 = cv::min(roi.y + roi.height + distance_thresh, img_frame.size().height);
      roi.x = cv::max(roi.x - distance_thresh, 0);
      roi.y = cv::max(roi.y - distance_thresh, 0);
      roi.width = x2 - roi.x;
      roi.height = y2 - roi.y;

      // Detect the target circle with lowered threshold
      LOG_INFO("Starting target detection...");
      isDetected = dtd.DetectTarget(img_frame, result_rect, roi, axis_ratio_track, size_thresh, max_axis_size, min_pixel_count, 
		min_contour_fit_score_track_, min_ellipse_fit_score_track_, min_white_pixels_ratio_track_, 
		max_edge_pixels_ratio_track_, whiteness_threshold_);
      LOG_INFO("Finished target detection...");
    }

    // Update the number of remaining tracking frames
    if (isDetected)
      tracking_remained_frames_ = track_frames_;
    
    return isDetected;
  }
  
  void onChange_min_contour_fit_score_detect(int, void*)
  {
    LOG_INFO("min_contour_fit_score_detect_ changed from %0.2lf to %0.2lf", min_contour_fit_score_detect_, min_contour_fit_score_detect / 100.0F);
    min_contour_fit_score_detect_ = min_contour_fit_score_detect / 100.0F;
  }
  
  void CalibrateParameters()
  {
    LOG_INFO("Starting calibration function...");

    if (CALIBRATEPARAMETERS == 1) 
    {
      min_contour_fit_score_detect = cvRound(min_contour_fit_score_detect_ * 100);
      min_contour_fit_score_track = cvRound(min_contour_fit_score_track_ * 100);
      min_ellipse_fit_score_detect = cvRound(min_ellipse_fit_score_detect_ * 100);
      min_ellipse_fit_score_track = cvRound(min_ellipse_fit_score_track_ * 100);
      min_white_pixels_ratio_detect = cvRound(min_white_pixels_ratio_detect_ * 100);
      min_white_pixels_ratio_track = cvRound(min_white_pixels_ratio_track_ * 100);
      max_edge_pixels_ratio_detect = cvRound(max_edge_pixels_ratio_detect_ * 100);
      max_edge_pixels_ratio_track = cvRound(max_edge_pixels_ratio_track_ * 100);
      whiteness_threshold = whiteness_threshold_;
      max_size_change = max_size_change_;
      track_frames = track_frames_;
      
      //Create trackbars in "Control" window
      cv::namedWindow("Calibrate Parameters", CV_WINDOW_AUTOSIZE);
      cv::createTrackbar("Contour Detect", "Calibrate Parameters", &min_contour_fit_score_detect, 100);
      cv::createTrackbar("Contour Track", "Calibrate Parameters", &min_contour_fit_score_track, 100);
      cv::createTrackbar("Ellipse Detect", "Calibrate Parameters", &min_ellipse_fit_score_detect, 100);
      cv::createTrackbar("Ellipse Track", "Calibrate Parameters", &min_ellipse_fit_score_track, 100);
      cv::createTrackbar("Whiteness Detect", "Calibrate Parameters", &min_white_pixels_ratio_detect, 100);
      cv::createTrackbar("Whiteness Track", "Calibrate Parameters", &min_white_pixels_ratio_track, 100);
      cv::createTrackbar("White Threshold", "Calibrate Parameters", &whiteness_threshold, 255);
      cv::createTrackbar("Max Change", "Calibrate Parameters", &max_size_change, 30);
      cv::createTrackbar("Track Frames", "Calibrate Parameters", &track_frames, 10);
    }
    
    CALIBRATEPARAMETERS = 2;

    waitKeyHandleHotkeys(1);
    
    if (min_contour_fit_score_detect != cvRound(min_contour_fit_score_detect_ * 100))
    {
      LOG_INFO("min_contour_fit_score_detect_ changed from %0.2lf to %0.2lf", min_contour_fit_score_detect_, min_contour_fit_score_detect / 100.0F);
      min_contour_fit_score_detect_ = min_contour_fit_score_detect / 100.0F;
    }
    if (min_contour_fit_score_track != cvRound(min_contour_fit_score_track_ * 100))
    {
      LOG_INFO("min_contour_fit_score_track_ changed from %0.2lf to %0.2lf", min_contour_fit_score_track_, min_contour_fit_score_track / 100.0F);
      min_contour_fit_score_track_ = min_contour_fit_score_track / 100.0F;
    }
    if (min_ellipse_fit_score_detect != cvRound(min_ellipse_fit_score_detect_ * 100))
    {
      LOG_INFO("min_ellipse_fit_score_detect_ changed from %0.2lf to %0.2lf", min_ellipse_fit_score_detect_, min_ellipse_fit_score_detect / 100.0F);
      min_ellipse_fit_score_detect_ = min_ellipse_fit_score_detect / 100.0F;
    }
    if (min_ellipse_fit_score_track != cvRound(min_ellipse_fit_score_track_ * 100))
    {
      LOG_INFO("min_ellipse_fit_score_track_ changed from %0.2lf to %0.2lf", min_ellipse_fit_score_track_, min_ellipse_fit_score_track / 100.0F);
      min_ellipse_fit_score_track_ = min_ellipse_fit_score_track / 100.0F;
    }
    if (min_white_pixels_ratio_detect != cvRound(min_white_pixels_ratio_detect_ * 100))
    {
      LOG_INFO("min_white_pixels_ratio_detect_ changed from %0.2lf to %0.2lf", min_white_pixels_ratio_detect_, min_white_pixels_ratio_detect / 100.0F);
      min_white_pixels_ratio_detect_ = min_white_pixels_ratio_detect / 100.0F;
    }
    if (min_white_pixels_ratio_track != cvRound(min_white_pixels_ratio_track_ * 100))
    {
      LOG_INFO("min_white_pixels_ratio_track_ changed from %0.2lf to %0.2lf", min_white_pixels_ratio_track_, min_white_pixels_ratio_track / 100.0F);
      min_white_pixels_ratio_track_ = min_white_pixels_ratio_track / 100.0F;
    }
    if (max_edge_pixels_ratio_detect != cvRound(max_edge_pixels_ratio_detect_ * 100))
    {
      LOG_INFO("max_edge_pixels_ratio_detect_ changed from %0.2lf to %0.2lf", max_edge_pixels_ratio_detect_, max_edge_pixels_ratio_detect / 100.0F);
      max_edge_pixels_ratio_detect_ = max_edge_pixels_ratio_detect / 100.0F;
    }
    if (max_edge_pixels_ratio_track != cvRound(max_edge_pixels_ratio_track_ * 100))
    {
      LOG_INFO("max_edge_pixels_ratio_track_ changed from %0.2lf to %0.2lf", max_edge_pixels_ratio_track_, max_edge_pixels_ratio_track / 100.0F);
      max_edge_pixels_ratio_track_ = max_edge_pixels_ratio_track / 100.0F;
    }
    if (whiteness_threshold != whiteness_threshold_)
    {
      LOG_INFO("whiteness_threshold_ changed from %i to %i", whiteness_threshold_, whiteness_threshold);
      whiteness_threshold_ = whiteness_threshold;
    }
    if (max_size_change != max_size_change_)
    {
      LOG_INFO("max_size_change_ changed from %i to %i", max_size_change_, max_size_change);
      max_size_change_ = max_size_change;
    }
    if (track_frames != track_frames_)
    {
      LOG_INFO("track_frames_ changed from %i to %i", track_frames_, track_frames);
      track_frames_ = track_frames;
    }

    LOG_INFO("Finished calibration...");
  }
    
  void SetParameters(
    double min_contour_fit_score_detect = 0.7, // Confidence to claim detection when the deck is not tracked
    double min_contour_fit_score_track = 0.3,  // Confidence to claim detection when the deck is tracked
    double min_ellipse_fit_score_detect = 0.95,// Confidence to claim detection when the deck is not tracked
    double min_ellipse_fit_score_track = 0.9,  // Confidence to claim detection when the deck is tracked
    double min_white_pixels_ratio_detect = 0.85,// Whiteness score for detection
    double min_white_pixels_ratio_track = 0.8, // Whiteness score for tracking
    double max_edge_pixels_ratio_detect = 0.3,
    double max_edge_pixels_ratio_track = 0.4,
    int whiteness_threshold = 140,
    int max_size_change = 10,        	 	// Maximum change in size while tracking
    int track_frames = 3            	 	// Number of frames to keep tracking when the target is not found
    )
  {
    LOG_INFO("Setting parameters for tracking...");

    min_contour_fit_score_detect_ = min_contour_fit_score_detect;
    min_contour_fit_score_track_ = min_contour_fit_score_track;
    min_ellipse_fit_score_detect_ = min_ellipse_fit_score_detect;
    min_ellipse_fit_score_track_ = min_ellipse_fit_score_track;
    min_white_pixels_ratio_detect_ = min_white_pixels_ratio_detect;
    min_white_pixels_ratio_track_ = min_white_pixels_ratio_track;
    max_edge_pixels_ratio_detect_ = max_edge_pixels_ratio_detect;
    max_edge_pixels_ratio_track_ = max_edge_pixels_ratio_track;
    whiteness_threshold_ = whiteness_threshold;
    max_size_change_ = max_size_change;
    track_frames_ = track_frames;

    LOG_INFO("Finished setting parameters for tracking...");
  }
  
  void LoadParameters(std::string calibration_filename)
  {
    // Read parameters from the file
    try
    {
      std::ifstream cal_file(calibration_filename.c_str());
      if (!cal_file.good())
      {
	ROS_ERROR("File \"%s\" does not exist!", calibration_filename.c_str());
	return;
      }

      cal_file 	>> min_contour_fit_score_detect_ 
		>> min_contour_fit_score_track_ 
		>> min_ellipse_fit_score_detect_ 
		>> min_ellipse_fit_score_track_
		>> min_white_pixels_ratio_detect_ 
		>> min_white_pixels_ratio_track_ 
		>> max_edge_pixels_ratio_detect_ 
		>> max_edge_pixels_ratio_track_
		>> whiteness_threshold_ 
		>> max_size_change_ 
		>> track_frames_;
      ROS_INFO("Decktrack: Successfully read calibration info.");
      cal_file.close();
    }
    catch (int e)
    {
      ROS_ERROR("Exception #%i: Error loading calibration data from file \"%s\"", e, calibration_filename.c_str());
    }
  }
  
  void SaveParameters(std::string calibration_filename)
  {
    try
    {
      std::ofstream cal_file(calibration_filename.c_str());
      if (!cal_file.good())
      {
	ROS_ERROR("Decktrack: Error writing to file \"%s\"!", calibration_filename.c_str());
	return;
      }

      cal_file 	<< min_contour_fit_score_detect_ << std::endl
		<< min_contour_fit_score_track_ << std::endl
		<< min_ellipse_fit_score_detect_ << std::endl
		<< min_ellipse_fit_score_track_<< std::endl
		<< min_white_pixels_ratio_detect_ << std::endl
		<< min_white_pixels_ratio_track_ << std::endl
		<< max_edge_pixels_ratio_detect_ << std::endl
		<< max_edge_pixels_ratio_track_ << std::endl
		<< whiteness_threshold_ << std::endl
		<< max_size_change_ << std::endl
		<< track_frames_ << std::endl;
		
      ROS_INFO("Decktrack: Successfully saved the calibration info.");
      cal_file.close();
    }
    catch (int e)
    {
      ROS_ERROR("Decktrack: Exception #%i: Error loading calibration data from file \"%s\"", e, calibration_filename.c_str());
    }
  }

public:
  Altimeter Laser;
   
  void waitKeyHandleHotkeys(int duration)
  {
    int pressed_key = cv::waitKey(duration) % 256;
    if (CALIBRATEPARAMETERS != 0 && (pressed_key == 's' || pressed_key == 'S'))
      SaveParameters(calib_output_filename_);
    else if (CALIBRATEPARAMETERS == 0 && (pressed_key == 'c' || pressed_key == 'C'))
      CALIBRATEPARAMETERS = 1;
    
    if (mbzirc::commons::Logger::Global.Enabled == false && (pressed_key == 'l' || pressed_key == 'L'))
    {
      ROS_WARN("Started logging decktrack module");
      if (mbzirc::commons::Logger::Global.Filename == "")
	LOG_INIT_UNIQUE("Decktrack", mbzirc::commons::System::GetHomeDir() + "/logs/decktrack");
      else
	mbzirc::commons::Logger::Global.Enabled = true;
    }
    else if (mbzirc::commons::Logger::Global.Enabled == true && (pressed_key == 'l' || pressed_key == 'L'))
    {
      ROS_WARN("Stopped logging decktrack module");
      mbzirc::commons::Logger::Global.Enabled = false;
    }
  }
  
  Tracker(ros::NodeHandle &nh, std::string calib_input_filename = "", std::string calib_output_filename = "")
  : Laser(nh)
  {
    // Set the parameters
    if (calib_input_filename != "")
      LoadParameters(calib_input_filename);
    else
      SetParameters(0.8, 0.5, 0.6, 0.6, 0.85, 0.8, 0.3, 0.4, 140, 15, 3);

    calib_output_filename_ = calib_output_filename;
    tracking_remained_frames_ = 0;
    initial_frame_scale = 1;
    frame_current_scale = initial_frame_scale;
    frame_scale_factor = 2;
    scale_size_threshold = 100;
  }
  
  bool TrackDeck(cv::Mat img_frame, cv::RotatedRect &out_target_rect, int &detected_scale)
  {
    LOG_INFO("Starting deck tracking...");
    try 
    {
      // Calibrate the parameters if needed
      if (CALIBRATEPARAMETERS > 0)
	CalibrateParameters();
      
      // Find the target
      bool isTracked = TrackTarget(img_frame, target_rect_, detected_scale);

      if (isTracked)
	out_target_rect = target_rect_;

      LOG_INFO("Finished deck tracking...");
      return isTracked;
    }
    catch (int e)
    {
      LOG_ERROR("Error in deck tracking...");
      ROS_ERROR("Decktrack: Tracker exception #%i", e);
      return false;
    }
  }
  
};

} // end namespace decktracker

} // end namespace mbzirc
