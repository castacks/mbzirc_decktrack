#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <boost/graph/graph_concepts.hpp>
#include <opencv_apps/RotatedRectStamped.h>
#include <mbzirc_commons/commons.h>
#include <mbzirc_decktrack/tracker.h>
#include <mbzirc_decktrack/options.h>
#include <mbzirc_decktrack/laser.h>
#include <vector>

cv_bridge::CvImagePtr cv_ptr_;
ros::Publisher target_pub_;
#ifdef SHOWPROCESSINGTIME
  ros::Time timer_;
  ros::Duration total_processing_time_;
  ros::Duration max_processing_time_;
#endif
mbzirc::decktracker::Tracker* dt;
int frame_number = 0;
mbzirc::commons::Logger::ILogger detection_log;
std::string image_save_path = mbzirc::commons::System::GetHomeDir() + "/output/";

#ifdef IMAGESEQUENCE
std::string result_file_prefix = "gnt_";
#endif

bool ProcessFrame(cv::Mat &img_frame, cv::RotatedRect &target_rect, ros::Duration &exec_time, int &detection_scale, std::string filename = "")
{
  try
  {
    LOG_INFO("Starting processing the frame...");
    // Track the deck
    LOG_INFO("Starting tracking target...");
    ros::Time exec_timer_start = ros::Time::now();
    bool isTracked = dt->TrackDeck(img_frame, target_rect, detection_scale);
    exec_time = ros::Time::now() - exec_timer_start;
    LOG_INFO("Finished tracking target...");

      
    if (isTracked)
    {
      // Draw the detection result on the frame
      if (mbzirc::commons::DebugLevel::ShowImages)
      {
	#ifdef SHOWHISTOGRAM
	  // Draw histogram of the detection
	  cv::Mat img_target_area = ((mbzirc::commons::Ellipse)target_rect).ExtractEllipseFromImage(img_frame, true);
	  cv::Mat img_hist = mbzirc::commons::OpenCVImageTools::HistogramImage(img_target_area, 2, 1, 255);
	  cv::imshow("Histogram", img_hist);
	#endif

	#ifdef DRAWDETECTION
	  cv::ellipse(img_frame, target_rect, cv::Scalar(0, 0, 255), 2, 8 );
	#endif

	#if defined(SAVEIMAGES) && defined(SAVEONLYDETECTEDIMAGES)
	  // Save images
      if (filename == "")
        mbzirc::commons::OpenCVImageTools::SaveImageToNumberedFile(img_frame, image_save_path + "$num$.jpg", frame_number);
    else
        cv::imwrite(image_save_path + result_file_prefix + filename, img_frame);
// 	  mbzirc::commons::EdgeImage edges;
// 	  edges.CalculateEdges(img_frame, 50, 200, 5, 100);
// 	  mbzirc::commons::OpenCVImageTools::SaveImageToNumberedFile(edges.Image, mbzirc::commons::System::GetHomeDir() + "/output/E$num$.jpg", frame_number);
	#endif
      }
    }
    
    if (mbzirc::commons::DebugLevel::ShowImages)
    {
      #if defined(SAVEIMAGES) && ! defined(SAVEONLYDETECTEDIMAGES)
	// Save images
	if (filename == "")
        mbzirc::commons::OpenCVImageTools::SaveImageToNumberedFile(img_frame, image_save_path + "$num$.jpg", frame_number);
    else
        cv::imwrite(image_save_path + result_file_prefix + filename, img_frame);
// 	mbzirc::commons::EdgeImage edges;
// 	edges.CalculateEdges(img_frame, 50, 200, 5, 100);
// 	mbzirc::commons::OpenCVImageTools::SaveImageToNumberedFile(edges.Image, mbzirc::commons::System::GetHomeDir() + "/output/E$num$.jpg", frame_number);
      #endif

      #ifdef SHOWDETECTEDAREA
        // Show the detected area of the image
        cv::Mat ellipse_region;
        if (((mbzirc::commons::Ellipse)target_rect).ExtractEllipseFromImage(img_frame, ellipse_region, true, false) == true)
            cv::imshow("Ellipse Image", ellipse_region);
      #endif
      
      if (img_frame.data == NULL)
	LOG_ERROR("Error! Frame data is null!");

      // Show the result image
      LOG_INFO("Showing detection");
      cv::imshow("Detection", img_frame);
      dt->waitKeyHandleHotkeys(1);
      
      LOG_INFO("Showed detection");
    }
    
    LOG_INFO("Finished processing the frame...");
    return isTracked;
  }
  catch (int e)
  {
    LOG_ERROR("Error in processing frame #%i", frame_number);
    ROS_ERROR("Decktrack: Node exception #%i", e);
    return false;
  }
}

void ReadAndProcessFrame(const sensor_msgs::ImageConstPtr& inp_msg)
{
  frame_number++;
  
  LOG_INFO("Starting reading frame #%i", frame_number);
  
  try { cv_ptr_ = cv_bridge::toCvCopy(inp_msg, sensor_msgs::image_encodings::BGR8);  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("Decktrack: cv_bridge exception: %s", e.what());
    return;
  }
  
  try
  {
    #ifdef SHOWDETECTIONS
      if (mbzirc::commons::DebugLevel::PrintInfo)
	ROS_INFO("Decktrack: Frame #%d...", frame_number);
    #endif

    #ifdef SHOWPROCESSINGTIME
      // Start timer
      ros::Time time_start = ros::Time::now();
      if (mbzirc::commons::DebugLevel::PrintInfo)
	LOG_INFO("Frequency = %0.1f Hz", 1.0F / (time_start - timer_).toSec());
      timer_ = time_start;
    #endif

//     // Remove black sides of the frame
    int frame_side_width = 0;
//     cv::Rect frame_roi = cv::Rect(frame_side_width, 0, 
// 				  cv_ptr_->image.cols - 2 * frame_side_width, cv_ptr_->image.rows);
//     mbzirc::commons::OpenCVImageTools::CropImage(cv_ptr_->image, cv_ptr_->image, frame_roi);
    
    cv::RotatedRect target_rect;
    ros::Duration exec_time;
    int detection_scale = -1;
    if (ProcessFrame(cv_ptr_->image, target_rect, exec_time, detection_scale) == true)
    {
      LOG_INFO("Starting publishing the target information...");
      LOG_INFO("Published target is: Center = (%0.1lf, %0.1lf) Size=(%0.1lf, %0.1lf) Angle=%0.1lf", 
	target_rect.center.x, target_rect.center.y, target_rect.size.width, target_rect.size.height, target_rect.angle);
        ILOG_INFO(detection_log, "%i %0.2lf %0.2lf %0.2lf %0.2lf %0.2lf", frame_number, target_rect.center.x, target_rect.center.y, target_rect.size.width, target_rect.size.height, target_rect.angle);

      
      
      // Publish the result to the topic
      opencv_apps::RotatedRectStamped msg;
      msg.rect.angle = target_rect.angle / 180.0 * CV_PI;
      msg.rect.center.x = target_rect.center.x + frame_side_width;
      msg.rect.center.y = target_rect.center.y;
      msg.rect.size.width = target_rect.size.width;
      msg.rect.size.height = target_rect.size.height;
      msg.header.frame_id = "/camera";
      msg.header.seq = frame_number;
      msg.header.stamp = inp_msg->header.stamp;
      target_pub_.publish<opencv_apps::RotatedRectStamped>(msg);
      LOG_INFO("Finished publishing the target information");
    }
    
    #ifdef SHOWPROCESSINGTIME
      if (mbzirc::commons::DebugLevel::PrintInfo)
      {
	ros::Duration frame_processing_time = ros::Time::now() - time_start;
	total_processing_time_ += frame_processing_time;
	if (frame_processing_time > max_processing_time_)
	  max_processing_time_ = frame_processing_time;
	LOG_INFO("Frame processing time: %0.0f ms", frame_processing_time.toSec() * 1000);
	
    int averaging_frames = 10;
    if (frame_number % averaging_frames == 0)
	{
	  ROS_INFO("Decktrack: Average processing time: %0.0f ms (Average FPS: %0.1f Hz)", total_processing_time_.toSec() * 1000 / averaging_frames, averaging_frames / total_processing_time_.toSec());
	  ROS_INFO("Decktrack: Largest processing time: %0.0f ms (Lowest FPS: %0.1f Hz)", max_processing_time_.toSec() * 1000, 1.0F / max_processing_time_.toSec());
        total_processing_time_.fromSec(0);
        max_processing_time_.fromSec(0);
	}
      }
    #endif
    LOG_INFO("Finished working on frame #%i", frame_number);

    // Stop if looking for a specific frame
    // if (frame_number == 47)
    //   cv::waitKey(0);
  }
  catch (int i)
  {
    LOG_ERROR("Error in processing frame #%i", frame_number);
    return;
  }
}

template <typename T> 
std::string NumberToString ( T Number )
{
    std::ostringstream ss;
    ss << Number;
    return ss.str();
}

bool SaveStringsToFile(std::string filename, std::vector<std::string> strings)
{
    try
    {
        // Open file stream to append
        std::ofstream file(filename.c_str());
        for (int i = 0; i < strings.size(); ++i)
            file << strings[i] << std::endl;
        return true;
    }
    catch (int e)
    {
        return false;
    }
}

  
int main(int argc, char** argv)
{
  // Initialize the node
  ros::init(argc, argv, "deck_track");
  ros::NodeHandle nh;
  image_transport::ImageTransport it_(nh);

  #ifdef SHOWPROCESSINGTIME
    timer_ = ros::Time::now();
  #endif
    
  // Set the debug level
  int DebugLevel;
  if (nh.getParam("/mbzirc/decktrack/debug", DebugLevel) == false)
  {
    DebugLevel = 0;
    ROS_INFO("Decktrack: Parameter '/mbzirc/decktrack/debug' is not defined! No output will be shown!");
  }
  mbzirc::commons::DebugLevel::SetDebugLevel(DebugLevel);

  int isLogEnabled;
  if (nh.getParam("/mbzirc/decktrack/log", isLogEnabled) == false)
    ROS_INFO("Decktrack: Parameter '/mbzirc/decktrack/log' is not defined! Logging is disabled!");
  else if (isLogEnabled == 1)
  {
    LOG_INIT_UNIQUE("Decktrack", mbzirc::commons::System::GetHomeDir() + "/logs/decktrack");
    ILOG_INIT_SIMPLE_NUMERIC_UNIQUE(detection_log, mbzirc::commons::System::GetHomeDir() + "/logs/detection");
  }
  else if (isLogEnabled == 2)
    LOG_INIT("Decktrack", mbzirc::commons::System::GetHomeDir() + "/logs/mbzirc");

  
  // Check for calibration mode
  if (nh.getParam("/mbzirc/decktrack/calib", CALIBRATEPARAMETERS) == false)
    CALIBRATEPARAMETERS = 0;
  else if (CALIBRATEPARAMETERS > 0)
    ROS_WARN("Decktrack: Starting calibration mode...");
    
  // Read calibration input and output filenames
  std::string calib_input_filename, calib_output_filename;
  nh.param<std::string>("/mbzirc/decktrack/calib_input", calib_input_filename, "");
  nh.param<std::string>("/mbzirc/decktrack/calib_output", calib_output_filename, "");

  if (CALIBRATEPARAMETERS > 0 && calib_output_filename == "")
    ROS_WARN("Decktrack: Calibration output file is not specified. Will not be able to save results.");
  
  dt = new mbzirc::decktracker::Tracker(nh, calib_input_filename, calib_output_filename);

  // Read the path for the image sequences
  std::string image_sequence_path;
  nh.param<std::string>("/mbzirc/decktrack/image_sequence_path", image_sequence_path, "");
  
  ros::Subscriber subLaser = nh.subscribe("/sf30/range", 1 , &Altimeter::LaserCB, &(dt->Laser));
  
  #ifdef SHOWCONTOURSONEBYONE
    cv::RotatedRect target_rect;
    cv::Mat img_frame = cv::imread(mbzirc::commons::System::GetHomeDir() + "/output/340.jpg");
    ProcessFrame(img_frame, target_rect);
  
  #elif defined IMAGESEQUENCE
    ROS_WARN("Reading image sequence from: %s", image_sequence_path.c_str());
    std::vector<std::string> dir_list = mbzirc::commons::System::ListSubDirectories(image_sequence_path);
    if (dir_list.empty())
        dir_list.push_back(image_sequence_path);
    for (int d = 0; d < dir_list.size(); ++d)
    {
        ROS_ERROR("Processing directory %d/%zu: %s", d + 1, dir_list.size(), dir_list[d].c_str());
        std::vector<std::string> file_list = mbzirc::commons::System::ListFiles(dir_list[d], "jpg", result_file_prefix);
        
        std::vector<std::string> sequence_info_file;
        std::vector<std::string> sequence_results_file;

        sequence_info_file.push_back("Processing directory  : \t" + dir_list[d]);
        
        ros::Duration total_exec_time = ros::Duration(0);
        ros::Duration total_positive_time = ros::Duration(0);
        ros::Duration total_negative_time = ros::Duration(0);
        ros::Duration max_exec_time = ros::Duration(0);
        ros::Duration max_positive_time = ros::Duration(0);
        ros::Duration max_negative_time = ros::Duration(0);
        ros::Duration min_exec_time = ros::Duration(1000);
        ros::Duration min_positive_time = ros::Duration(1000);
        ros::Duration min_negative_time = ros::Duration(1000);
        int positive_frames = 0, negative_frames = 0;
        
        sequence_results_file.push_back("OriginalFile\tProcessedFile\tExecTime\tTargetFound\tTargetCenterX\tTargetCenterY\tTargetWidth\tTargetHeight\tTargetAngle\tDetectionScale");
        for (int f = 0; f < file_list.size(); ++f)
        {
            std::ostringstream results_stream;
            results_stream << file_list[f] << '\t' << (result_file_prefix + file_list[f]) << '\t';
            
            //ROS_INFO("Detecting ellipses on file: %s", (dir_list[d] + file_list[f]).c_str());
            image_save_path = dir_list[d];
            cv::RotatedRect target_rect;
            cv::Mat img_frame = cv::imread(dir_list[d] + file_list[f]);

            ros::Duration exec_time_file;
            int detection_scale = -1;
            bool is_target_found = ProcessFrame(img_frame, target_rect, exec_time_file, detection_scale, file_list[f]);

            results_stream << exec_time_file.toSec() << '\t' << (is_target_found?1:0) << '\t' << 
                target_rect.center.x << '\t' << target_rect.center.y << '\t' << target_rect.size.width << 
                '\t' << target_rect.size.height << '\t' << target_rect.angle << '\t' << detection_scale;
                
            sequence_results_file.push_back(results_stream.str());
            
            total_exec_time += exec_time_file;
            max_exec_time = std::max(max_exec_time, exec_time_file);
            min_exec_time = std::min(min_exec_time, exec_time_file);
            if (is_target_found)
            {
                positive_frames++;
                total_positive_time += exec_time_file;
                max_positive_time = std::max(max_positive_time, exec_time_file);
                min_positive_time = std::min(min_positive_time, exec_time_file);
            }
            else
            {
                negative_frames++;
                total_negative_time += exec_time_file;
                max_negative_time = std::max(max_negative_time, exec_time_file);
                min_negative_time = std::min(min_negative_time, exec_time_file);
            }
            cv::waitKey(1);
        }
        sequence_info_file.push_back("Total number of files : \t" + NumberToString(file_list.size()));
        sequence_info_file.push_back("Number of positives   : \t" + NumberToString(positive_frames));
        sequence_info_file.push_back("Number of negatives   : \t" + NumberToString(negative_frames));
        
        sequence_info_file.push_back("");
        sequence_info_file.push_back("Average execution time: \t" + NumberToString(total_exec_time.toSec() / (positive_frames + negative_frames)));
        sequence_info_file.push_back("Average positives time: \t" + NumberToString(total_positive_time.toSec() / positive_frames));
        sequence_info_file.push_back("Average negatives time: \t" + NumberToString(total_negative_time.toSec() / negative_frames));

        sequence_info_file.push_back("");
        sequence_info_file.push_back("Maximum execution time: \t" + NumberToString(max_exec_time.toSec()));
        sequence_info_file.push_back("Maximum positives time: \t" + NumberToString(max_positive_time));
        sequence_info_file.push_back("Maximum negatives time: \t" + NumberToString(max_negative_time));

        sequence_info_file.push_back("");
        sequence_info_file.push_back("Minimum execution time: \t" + NumberToString(min_exec_time));
        sequence_info_file.push_back("Minimum positives time: \t" + NumberToString(min_positive_time));
        sequence_info_file.push_back("Minimum negatives time: \t" + NumberToString(min_negative_time));
        
        sequence_info_file.push_back("");
        sequence_info_file.push_back("Calibration file      : \t" + calib_input_filename);
        sequence_info_file.push_back("");
        sequence_info_file.push_back("Calibration parameters:");
        
        std::ifstream cal_file(calib_input_filename.c_str());
        std::string temp_line;
        if (cal_file.good())
        {
            while (std::getline(cal_file, temp_line))
                sequence_info_file.push_back(temp_line);
            cal_file.close();
        }
        
        SaveStringsToFile(dir_list[d] + "all_info_gnt.txt", sequence_info_file);
        SaveStringsToFile(dir_list[d] + "all_results_gnt.txt", sequence_results_file);
    }
  #else
    // Initialize publisher and subscriber
    image_transport::Subscriber image_sub_ = it_.subscribe("/dji_sdk/image_raw", 1, &ReadAndProcessFrame);
    target_pub_ = nh.advertise<opencv_apps::RotatedRectStamped>("/deck_track/target", 1);
  #endif
  
  ros::spin();
  return 0;
}
